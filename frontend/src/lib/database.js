import sqlite from "better-sqlite3";

const db = new sqlite("./db.sqlite");

const schema = `BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS "events" (
\t"id"\tINTEGER NOT NULL UNIQUE,
\t"info"\tTEXT NOT NULL,
\t"public"\tINTEGER NOT NULL,
\t"type"\tTEXT NOT NULL DEFAULT 'webrtc',
\tPRIMARY KEY("id" AUTOINCREMENT)
);
INSERT OR IGNORE INTO "events" ("id","info","public","type") VALUES (1,'{"title":"Testveranstaltung","description": "Diese Veranstaltung ist nur ein Test!","image": null}',1,'webrtc');

COMMIT;
`;

db.exec(schema);

export default {
    getAllEvents: async () => await db.prepare("SELECT * FROM events").all(),
    getPublicEvents: async () => (await db.prepare("SELECT * FROM events WHERE events.public = 1").all()),
    addEvent: async (info, isPublic, type) => await db.prepare("INSERT INTO events(info, public, type) VALUES (?, ?, ?)").run(JSON.stringify(info), isPublic ? 1 : 0, type),
    updateEvent: async (id, info, isPublic, type) => await db.prepare("UPDATE events SET info = ?, public = ?, type = ? WHERE events.id = ?").run(JSON.stringify(info), isPublic ? 1 : 0, type, id),
    deleteEvent: async (id) => await db.prepare("DELETE FROM events WHERE events.id = ?").run(id),

    //getAllEvents: async () => await db.prepare('SELECT * FROM events').all(),
};
