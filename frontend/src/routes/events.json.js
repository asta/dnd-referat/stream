import db from "$lib/database.js";
import Joi from "joi";

export async function get() {
    const events = await db.getPublicEvents();
    if (events) {
        return {
            body: {
                events: events.map(e => ({id: e.id, info: JSON.parse(e.info), type: e.type, public: e.public}))
            }
        };
    }
}

export async function post(request) {
    const schema = Joi.object({
        id: Joi.number(),
        info: Joi.object({
            title: Joi.string().required(),
            description: Joi.string().required(),
            image: Joi.string().allow(null).required()
        }).required(),
        public: Joi.alternatives().try(1, 0).required(),
        type: Joi.string().required()
    });
    try {
        Joi.attempt(request.body, schema);
    } catch (err) {
        console.log(err);
        return {
            status: 400,
            body: {
                reasons: err.details.map(detail => `Validation Error at path "${detail.path}": ${detail.message}`).join("\n")
            }
        };
    }
    if (request.body.id) {
        console.log(`update event`);
        await db.updateEvent(request.body.id, request.body.info, request.body.public, request.body.type);
    } else {
        console.log(`add event`);
        await db.addEvent(request.body.info, request.body.public, request.body.type);
    }
    return {
        status: 201
    };
}

export async function del(request) {
    const schema = Joi.object({
        id: Joi.number()
    });
    try {
        Joi.attempt(request.body, schema);
    } catch (err) {
        console.log(err);
        return {
            status: 400,
            body: {
                reasons: err.details.map(detail => `Validation Error at path "${detail.path}": ${detail.message}`).join("\n")
            }
        };
    }
    await db.deleteEvent(request.body.id);
    return {
        status: 200
    };
}
