// production = false;
// module.exports = {
//     purge: {
//         content: [
//             "./src/**/*.svelte",
//         ],
//         enabled: production // disable purge in dev
//     },
//     darkMode: false, // or 'media' or 'class'
//     mode: "jit",
//     theme: {
//         extend: {},
//     },
//     variants: {
//         extend: {},
//     },
//     plugins: [
//         require("daisyui")
//     ],
//     future: {
//         purgeLayersByDefault: true,
//         removeDeprecatedGapUtilities: true,
//     },
// };

module.exports = {
    mode: 'jit',
    purge: {
        content: [
            './src/**/*.{html,js,svelte,ts}',
        ],
        options: {
            safelist: [
                /data-theme$/,
            ]
        },
    },
    plugins: [
        require('daisyui')
    ],
}