```
../../ansible-role-ome/scripts/easy_signed_policy_url_generator.sh "<Secret>" 1209600 c103-193.cloud.gwdg.de 3334 app_1080p30 ophase
```
1209600 Sekunden = 2 Wochen

```

==>> OBS Settings <<==
Server: rtmp://c103-193.cloud.gwdg.de:1935/app_1080p30
Key: ophase?xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx



==> Player Settings <<==
WEBRTC: wss://c103-193.cloud.gwdg.de:3334/app_1080p30/ophase
#HLS: https://c103-193.cloud.gwdg.de/app_1080p30/ophase/playlist.m3u8
```

[Stream Demo](https://demo.ovenplayer.com/#sources=%5B%7B%22default%22%3Afalse%2C%22id%22%3A0%2C%22label%22%3A%22O-Phase%20(Edge%201)%22%2C%22file%22%3A%22wss%3A%2F%2Fc103-250.cloud.gwdg.de%3A3334%2Fapp_1080p30%2Fophase%22%2C%22type%22%3A%22webrtc%22%7D%2C%7B%22default%22%3Afalse%2C%22id%22%3A1%2C%22label%22%3A%22O-Phase%20(Edge%201)%20Bypass%22%2C%22file%22%3A%22wss%3A%2F%2Fc103-250.cloud.gwdg.de%3A3334%2Fapp_1080p30%2Fophase_bypass%22%2C%22type%22%3A%22webrtc%22%7D%2C%7B%22default%22%3Afalse%2C%22id%22%3A2%2C%22label%22%3A%22O-Phase%20(Edge%202)%22%2C%22file%22%3A%22wss%3A%2F%2Fc103-224.cloud.gwdg.de%3A3334%2Fapp_1080p30%2Fophase%22%2C%22type%22%3A%22webrtc%22%7D%2C%7B%22default%22%3Afalse%2C%22id%22%3A3%2C%22label%22%3A%22O-Phase%20(Edge%202)%20Bypass%22%2C%22file%22%3A%22wss%3A%2F%2Fc103-224.cloud.gwdg.de%3A3334%2Fapp_1080p30%2Fophase_bypass%22%2C%22type%22%3A%22webrtc%22%7D%2C%7B%22default%22%3Afalse%2C%22id%22%3A4%2C%22label%22%3A%22O-Phase%20(Edge%203)%22%2C%22file%22%3A%22wss%3A%2F%2Fc103-205.cloud.gwdg.de%3A3334%2Fapp_1080p30%2Fophase%22%2C%22type%22%3A%22webrtc%22%7D%2C%7B%22default%22%3Afalse%2C%22id%22%3A5%2C%22label%22%3A%22O-Phase%20(Edge%203)%20Bypass%22%2C%22file%22%3A%22wss%3A%2F%2Fc103-205.cloud.gwdg.de%3A3334%2Fapp_1080p30%2Fophase_bypass%22%2C%22type%22%3A%22webrtc%22%7D%2C%7B%22default%22%3Afalse%2C%22id%22%3A6%2C%22label%22%3A%22O-Phase%20(Edge%204)%22%2C%22file%22%3A%22wss%3A%2F%2Fc103-222.cloud.gwdg.de%3A3334%2Fapp_1080p30%2Fophase%22%2C%22type%22%3A%22webrtc%22%7D%2C%7B%22default%22%3Afalse%2C%22id%22%3A7%2C%22label%22%3A%22O-Phase%20(Edge%204)%20Bypass%22%2C%22file%22%3A%22wss%3A%2F%2Fc103-222.cloud.gwdg.de%3A3334%2Fapp_1080p30%2Fophase_bypass%22%2C%22type%22%3A%22webrtc%22%7D%2C%7B%22default%22%3Afalse%2C%22id%22%3A8%2C%22label%22%3A%22O-Phase%20(Edge%205)%22%2C%22file%22%3A%22wss%3A%2F%2Fc103-227.cloud.gwdg.de%3A3334%2Fapp_1080p30%2Fophase%22%2C%22type%22%3A%22webrtc%22%7D%2C%7B%22default%22%3Afalse%2C%22id%22%3A9%2C%22label%22%3A%22O-Phase%20(Edge%205)%20Bypass%22%2C%22file%22%3A%22wss%3A%2F%2Fc103-227.cloud.gwdg.de%3A3334%2Fapp_1080p30%2Fophase_bypass%22%2C%22type%22%3A%22webrtc%22%7D%2C%7B%22default%22%3Afalse%2C%22id%22%3A10%2C%22label%22%3A%22O-Phase%20(Edge%206)%22%2C%22file%22%3A%22wss%3A%2F%2Fc103-195.cloud.gwdg.de%3A3334%2Fapp_1080p30%2Fophase%22%2C%22type%22%3A%22webrtc%22%7D%2C%7B%22default%22%3Afalse%2C%22id%22%3A11%2C%22label%22%3A%22O-Phase%20(Edge%206)%20Bypass%22%2C%22file%22%3A%22wss%3A%2F%2Fc103-195.cloud.gwdg.de%3A3334%2Fapp_1080p30%2Fophase_bypass%22%2C%22type%22%3A%22webrtc%22%7D%2C%7B%22default%22%3Afalse%2C%22id%22%3A12%2C%22label%22%3A%22O-Phase%20(Origin)%22%2C%22file%22%3A%22wss%3A%2F%2Fc103-193.cloud.gwdg.de%3A3334%2Fapp_1080p30%2Fophase%22%2C%22type%22%3A%22webrtc%22%7D%2C%7B%22default%22%3Afalse%2C%22id%22%3A13%2C%22label%22%3A%22O-Phase%20(Origin)%20Bypass%22%2C%22file%22%3A%22wss%3A%2F%2Fc103-193.cloud.gwdg.de%3A3334%2Fapp_1080p30%2Fophase_bypass%22%2C%22type%22%3A%22webrtc%22%7D%2C%7B%22default%22%3Afalse%2C%22id%22%3A14%2C%22label%22%3A%22O-Phase%20(Origin)%20HLS%22%2C%22file%22%3A%22https%3A%2F%2Fc103-193.cloud.gwdg.de%2Fapp_1080p30%2Fophase%2Fplaylist.m3u8%22%2C%22type%22%3A%22hls%22%7D%5D&lowLatency=false&liveDelay=false)
