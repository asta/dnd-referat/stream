https://airensoft.gitbook.io/ovenmediaengine/

```
ansible-galaxy collection install -r requirements.yml
```

```
ansible-playbook -i ../../asta-ansible/inventory/ -i inventory/ playbooks/main.yml --ask-vault-pass --forks 50
ansible-playbook -i ../../asta-ansible/inventory/ -i inventory/ playbooks/main.yml --ask-vault-pass --skip-tags "initial-install,run-certbot"
```
